'use strict';

/**
 * Creates a new representation of a container.
 * @class DockerContainer
 *
 * @param {Object} Object containing container information
 * @param {string} opts.image Name of the image
 * @param {string} opts.cmd Command to be executed
 * @param {string} opts.name Name for the container
 */

var DockerContainer = function (opts) {
    this.image = opts.image || '';
    this.cmd = opts.cmd || [];
    this.name = opts.name || '';
};

module.exports = DockerContainer;