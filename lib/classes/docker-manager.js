'use strict';

var Docker = require('dockerode');
var q      = require('q');
var fs     = require('fs');

require('colors');

var DockerManager = function () {
    this.docker = undefined;

    /**
     * Starts a new connection with the local or
     * a remote docker daemon.
     *
     * @function
     *
     * @throws {Error} Docker Host environment variable's
     *                 syntax is wrong.
     * @throws {Error} No protocol has been specified.
     */
    function init(that) {
        if (typeof process.env.DOCKER_HOST !== 'undefined') {
            var DOCKER_HOST = process.env.DOCKER_HOST;

            /*
             * The variable dockerHostComponents is expected to be an array
             * with the following components
             *
             * [ 'tcp', '192.168.99.100', '2376' ]
             */
            var dockerHostComponents = DOCKER_HOST.replace('//', '').split(':');

            if (dockerHostComponents.length !== 3) {
                process.stdout.write('DOCKER_HOST syntax is wrong! '.bold.red);
                process.stdout.write('Define it as follows: \n'.bold.red);
                console.log('<protocol>://<host>:<host-port>'.red);

                throw new Error('DOCKER_HOST syntax is wrong');
            }

            var certPath = process.env.DOCKER_CERT_PATH;

            that.docker = new Docker({
                protocol: 'https',
                host: dockerHostComponents[1],
                port: dockerHostComponents[2],
                ca: fs.readFileSync(certPath + '/ca.pem'),
                cert: fs.readFileSync(certPath + '/cert.pem'),
                key: fs.readFileSync(certPath + '/key.pem'),
            });
        } else if (typeof process.env.DOCKER_SOCK !== 'undefined') {
            that.docker = new Docker({socketPath: process.env.DOCKER_SOCK});
        } else {
            process.stdout.write('The docker daemon needs to either '.bold.red);
            process.stdout.write('bind to a Unix socket or '.bold.red);
            process.stdout.write('to a TCP port!\n\n'.bold.red);

            throw new Error('Daemon needs to bind to a protocol');
        }
    }

    /**
     * Creates a new container based on an image. Note that if
     * the giving container specifies an inexistent image,
     * the container will not be spun up. Unfortunately, Docker Remote
     * API does not allow us to download the image as you run the container.
     * We'll have a workaround in a near future.
     *
     * @function
     *
     * @param {DockerContainer} container - The container to be created
     *
     * @returns {Promise} This function returns a promise at first, but later
     *                    it will return the container's hash if it
     *                    was created, or it will return an error.
     */
    this.createContainer = function (container) {
        var deffered = q.defer();

        var containerInfo = {
            Image: container.image,
            name: container.name,
        };

        this.docker.createContainer(containerInfo, function (err, cont) {
            if (err) {
                deffered.reject(err);

                return;
            }

            var containerId = cont.id;

            cont.start(function (err, data) {
                if (err) {
                    deffered.reject(err);

                    return;
                }

                deffered.resolve(containerId, data);
            });
        });

        return deffered.promise;
    };

    init(this);
};

module.exports = DockerManager;