'use strict';

var chai      = require('chai');
var sinon     = require('sinon');
var sinonChai = require('sinon-chai');

chai.should();
chai.use(sinonChai);

var DockerManager = require('../').DockerManager;
var DockerContainer = require('../').DockerContainer;

describe('Running tests on Docker Manager', function () {
    describe('.constructor()', function () {
        it('Start a new connection using TCP protocol', function (done) {
            var dockerManager = new DockerManager();

            dockerManager.should.be.an('object');
            done();
        });
    });

    describe('.createContainer()', function () {
        var dockerManager;

        before(function () {
            dockerManager = new DockerManager();
        });

        it('create a container and return its hash', function (done) {
            var callback = sinon.spy();
            var container = new DockerContainer({image: 'hello-world'});
            var promise = dockerManager.createContainer(container);

            promise
                .then(function (containerId) {
                    containerId.should.be.a('string');
                    done();
                }, callback)
                .done(function () {
                    callback.should.have.callCount(0);
                });
        });

        it('fail to create when image does not exist', function (done) {
            var callback = sinon.spy();
            var container = new DockerContainer({image: 'inexistent'});
            var promise = dockerManager.createContainer(container);
            promise
                .then(callback, function (err) {
                    err.statusCode.should.equal(404);
                    done();
                })
                .done(function () {
                    callback.should.have.callCount(0);
                });
        });
    });
});

