'use strict';

var chai      = require('chai');
var sinonChai = require('sinon-chai');

chai.should();
chai.use(sinonChai);

var DockerContainer = require('../').DockerContainer;

describe('Running tests on Carbono Container', function () {

    describe('.constructor()', function () {

        it('create container with parameters', function (done) {
            var container = new DockerContainer({
                image: 'hello-world',
                cmd: ['bash', '-c', 'uname -a'],
                name: 'hello',
            });

            container.image.should.equal('hello-world');
            container.cmd.should.deep.equal(['bash', '-c', 'uname -a']);
            container.name.should.equal('hello');
            done();
        });

        it('create container with empty parameters', function (done) {
            var container = new DockerContainer({
                image: '',
                cmd: [],
                name: '',
            });

            container.image.should.equal('');
            container.cmd.should.deep.equal([]);
            container.name.should.equal('');
            done();
        });
    });
});

